﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageMenu : MonoBehaviour
{
	#region Members

	public GameObject LangSection;
	public GameObject BannerSection;

	#endregion


	#region Callback

	public void SelectLanguage(int id)
	{
		LangSection.SetActive(false);
		//
		PlayerProfile.Data.GameLanguage = (Novulo.Localisation.Translator.ELang)id;
		PlayerProfile.Get.SetDirty();
		//
		Novulo.Utility.CoroutineHelper.Start(this, LoadCoroutine());
	}

	#endregion


	#region Coroutine

	private IEnumerator LoadCoroutine()
	{
		BannerSection.SetActive(true);
		//
		yield return new WaitForSeconds(1f);
		//
		GameManager.Get.LoadGame();
	}

	#endregion
}
