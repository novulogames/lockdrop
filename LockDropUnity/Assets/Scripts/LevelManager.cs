﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
	#region Events

	public delegate void LevelCreatedCallback();
	public static LevelCreatedCallback OnLevelCreated;

	public delegate void LevelStartCallback();
	public static LevelStartCallback OnLevelStart;

	public delegate void LevelEndedCallback();
	public static LevelEndedCallback OnLevelEnded;
	
	#endregion


	#region Members

	public GameObject Group;
	[Space]
	public float TimeAttackDuration;
	[Space]
	public GameObject FloorPrefab;
	public float FloorOffset;

	private GameObject m_floorParent = null;
	private List<GameObject> m_floors = new List<GameObject>();

	#endregion


	#region Properties

	public static LevelManager Get { get; private set; } = null;

	public Types.EGameMode GameMode { get; private set; } = Types.EGameMode.Noof;
	public int CurrentLevel { get; private set; } = 0;
	public double CurrentTime { get; private set; } = 0f;
	public float GameTimeScale { get; private set; } = 0f;
	public bool GamePaused { get; set; } = true;
	#endregion


	#region MonoBehaviour

	private void Awake()
	{
		Get = this;
		//
		MainMenu.OnGameSelected += GameModeSelected;
	}

	private void OnDestroy()
	{
		MainMenu.OnGameSelected -= GameModeSelected;
		//
		Get = null;
	}

	private void Update()
	{
		if (GamePaused)
			return;

		float dt = Time.deltaTime * GameTimeScale;
		float dir = (GameMode == Types.EGameMode.FreeRun) ? 1f : -1f;
		CurrentTime += (dt * dir);
	
		if (GameMode == Types.EGameMode.TimeAttack)
		{
			CurrentTime = Mathf.Max(0f, (float)CurrentTime);
			if (CurrentTime == 0f)
				EndTimeAttack();
		}
	}

	#endregion


	#region Methods

	public void EndTimeAttack()
	{
		GamePaused = true;
		GameTimeScale = 0f;
		//
		OnLevelEnded?.Invoke();
	}

	#endregion


	#region Internal

	private void ClearLevel()
	{
		if (m_floorParent != null)
		{
			// Clean
			Destroy(m_floorParent);
			m_floors.Clear();
		}
	}

	private void GenerateLevel()
	{
		ClearLevel();

		//

		m_floorParent = new GameObject("FloorParent");
		m_floorParent.transform.parent = this.transform;

		for (int idx = 0; idx < CurrentLevel; ++idx)
		{
			GameObject go = Instantiate<GameObject>(FloorPrefab, m_floorParent.transform);
			go.transform.position = new Vector3(0f, idx * FloorOffset + (0.5f * FloorOffset), 0f);
		}

		Vector3 pos = BallController.Get.transform.position;
		pos.y = (CurrentLevel + 1) * FloorOffset;
		BallController.Get.transform.position = pos;

		OnLevelCreated?.Invoke();
	}

	#endregion


	#region Callback

	public void GameModeSelected(Types.EGameMode mode)
	{
		InGameMenu.OnLeaveLevel += LeaveLevel;
		CountdownController.OnCountdownOver += CountdownOver;
		BaseController.OnLevelComplete += LevelComplete;
		//
		GameMode = mode;
		switch(GameMode)
		{
			case Types.EGameMode.TimeAttack: CurrentTime = TimeAttackDuration * 60f; break;
			case Types.EGameMode.FreeRun: CurrentTime = 0f; break;
		}
		//
		Group.SetActive(true);
		LaunchNext();
		//
		OnLevelStart?.Invoke();
	}

	private void LeaveLevel()
	{
		InGameMenu.OnLeaveLevel -= LeaveLevel;
		CountdownController.OnCountdownOver -= CountdownOver;
		BaseController.OnLevelComplete -= LevelComplete;
		//
		Novulo.Social.SocialManager.Get.PublishLeaderboardEntry(
			GameMode == Types.EGameMode.FreeRun
				? ""
				: "",
			CurrentLevel
			);
		//
		if (GameMode == Types.EGameMode.FreeRun)
		{
			if (PlayerProfile.Data.FreeModeBest < (uint)CurrentLevel)
			{
				PlayerProfile.Data.FreeModeBest = (uint)CurrentLevel;
				PlayerProfile.Data.FreeBestTime = CurrentTime;
			}
			else if (	PlayerProfile.Data.FreeModeBest == (uint)CurrentLevel &&
						PlayerProfile.Data.FreeBestTime > CurrentTime)
			{
				PlayerProfile.Data.FreeBestTime = CurrentTime;
			}
			Novulo.Social.SocialManager.Get.PublishLeaderboardEntry(GPGSIds.leaderboard_free_mode, (int)PlayerProfile.Data.FreeModeBest);
		}
		else if (GameMode == Types.EGameMode.TimeAttack)
		{
			PlayerProfile.Data.TimedAttackBest = (uint)Mathf.Max(PlayerProfile.Data.TimedAttackBest, CurrentLevel);
			Novulo.Social.SocialManager.Get.PublishLeaderboardEntry(GPGSIds.leaderboard_time_attack, (int)PlayerProfile.Data.TimedAttackBest);
		}

		PlayerProfile.Get.SetDirty();
		//
		ClearLevel();
		Group.SetActive(false);
		//
		GameMode = Types.EGameMode.Noof;
		CurrentLevel = 0;
		CurrentTime = 0f;
		GamePaused = true;
		GameTimeScale = 0f;
		//
		OnLevelEnded?.Invoke();
	}

	private void CountdownOver()
	{
		GamePaused = false;
		GameTimeScale = 1f;
	}

	private void LevelComplete()
	{
		// Add 10 seconds on time attack
		if (GameMode == Types.EGameMode.TimeAttack)
			CurrentTime += 10;

		GamePaused = true;
		GameTimeScale = 0f;
		//
		StartCoroutine(DelayComplete());
	}

	#endregion


	#region Internal
	
	private void LaunchNext()
	{
		++CurrentLevel;
		GenerateLevel();
		//
		CountdownController.Get.StartCountdown();
	}

	#endregion


	#region Coroutine

	private IEnumerator DelayComplete()
	{
		yield return new WaitForSeconds(0.5f);
		LaunchNext();
	}

	#endregion
}
