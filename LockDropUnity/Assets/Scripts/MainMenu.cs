﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
	#region Events

	public delegate void GameModeSelected(Types.EGameMode mode);
	public static GameModeSelected OnGameSelected;

	#endregion


	#region Members

	public GameObject Group;
	[Space]
	public GameObject RemoveAdsButton;
	[Space]
	public Text FreeBestLabel;
	public Text TimedBestLabel;

	#endregion


	#region MonoBehaviour

	private void Awake()
	{
		LevelManager.OnLevelEnded += LevelEnded;
		Novulo.Store.IAPManager.OnStoreInit += StoreInit;
		Novulo.Store.IAPManager.OnIAPPurchaseComplete += IAPComplete;
	}

	private void OnDestroy()
	{
		LevelManager.OnLevelEnded -= LevelEnded;
		Novulo.Store.IAPManager.OnStoreInit -= StoreInit;
		Novulo.Store.IAPManager.OnIAPPurchaseComplete -= IAPComplete;
	}

	private void Start()
	{
		BannerManager.Get.StartBanner();
		UpdateButtons();
		LevelEnded();
		//
		SocialSignIn();
	}

	#endregion


	#region Internal

	private void UpdateButtons()
	{
		RemoveAdsButton.GetComponent<Button>().interactable = Novulo.Store.IAPManager.Get.IsInitialised;
		RemoveAdsButton.SetActive(PlayerProfile.Data.RemoveAds == false);
	}

	private void UpdateLabels()
	{
		FreeBestLabel.text = Novulo.Localisation.Translator.Get.GetTranslation("id_free_best") + ": " + PlayerProfile.Data.FreeModeBest + " @ " + GameManager.Get.FormatToUI(PlayerProfile.Data.FreeBestTime);
		TimedBestLabel.text = Novulo.Localisation.Translator.Get.GetTranslation("id_time_best") + ": " + PlayerProfile.Data.TimedAttackBest;
	}

	#endregion


	#region Callback

	public void FreeRunModePressed()
	{
		OnGameSelected?.Invoke(Types.EGameMode.FreeRun);
		Group.SetActive(false);
	}

	public void TimeAttackModePressed()
	{
		OnGameSelected?.Invoke(Types.EGameMode.TimeAttack);
		Group.SetActive(false);
	}

	public void LeaderboardsPressed()
	{
		if (!Novulo.Social.SocialManager.Get.IsLoggedIn)
			SocialSignIn();

		Novulo.Social.SocialManager.Get.ShowLeaderboardsUI();
	}

	public void SocialSignIn()
	{
		if (!Novulo.Social.SocialManager.Get.IsLoggedIn)
			Novulo.Social.SocialManager.Get.LogIn();
	}


	public void RemoveAdsPressed()
	{
		if (!RemoveAdsButton.GetComponent<Button>().interactable)
			return;

		RemoveAdsButton.GetComponent<Button>().interactable = false;
		Novulo.Store.IAPManager.Get.Purchase("lockdrop.removeads");
	}
	
	public void ChangeLanguage()
	{
		SceneHandler.LoadScene(SceneHandler.EScene.Language);
	}

	//

	private void LevelEnded()
	{
		Group.SetActive(true);
		UpdateLabels();
	}

	//

	private void StoreInit(bool success)
	{
		UpdateButtons();
	}

	private void IAPComplete(string IAPName, bool success)
	{
		if (success)
			GameManager.Get.ProcessIAP(IAPName);

		UpdateButtons();
	}

	#endregion
}
