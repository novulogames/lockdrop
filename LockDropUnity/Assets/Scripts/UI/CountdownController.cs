﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountdownController : MonoBehaviour
{
	#region Events

	public delegate void CountdownOverCallback();
	public static CountdownOverCallback OnCountdownOver;

	#endregion


	#region Members

	private Animator m_anim = null;

	#endregion


	#region Properties

	public static CountdownController Get { get; private set; } = null;

	#endregion


	#region MonoBehaviour

	private void Awake()
	{
		m_anim = GetComponent<Animator>();
		Get = this;
	}

	private void OnDestroy()
	{
		Get = null;
	}

	#endregion


	#region Methods

	public void StartCountdown()
	{
		m_anim.SetTrigger("go");
	}

	#endregion


	#region Callback

	private void TickAnimEvent()
	{
		AudioManager.Get.PlayTickSound();
	}

	private void CountdownOverAnimEvent()
	{
		AudioManager.Get.PlayGoSound();
		OnCountdownOver?.Invoke();
	}

	#endregion
}
