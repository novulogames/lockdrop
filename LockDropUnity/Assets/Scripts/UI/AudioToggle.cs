﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioToggle : MonoBehaviour
{
	#region Members

	public Image ButtonImage;
	[Space]
	public GameObject OnSprite;
	public GameObject OffSprite;
	[Space]
	public Color OnColor;
	public Color OffColor;
	[Space]
	public AudioSource MusicSource;

	#endregion


	#region MonoBehaviour

	private void OnEnable()
	{
		UpdateToggle();
	}

	#endregion


	#region Internal

	private void UpdateToggle()
	{
		MusicSource.mute = (!PlayerProfile.Data.SoundOn);
		//
		OnSprite.SetActive(PlayerProfile.Data.SoundOn);
		OffSprite.SetActive(!PlayerProfile.Data.SoundOn);
		//
		ButtonImage.color = (PlayerProfile.Data.SoundOn) ? OnColor : OffColor;
	}

	#endregion


	#region Callback

	public void TogglePressed()
	{
		PlayerProfile.Data.SoundOn = !PlayerProfile.Data.SoundOn;
		PlayerProfile.Get.SetDirty();
		//
		UpdateToggle();
	}

	#endregion
}
