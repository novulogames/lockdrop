﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BannerManager : MonoBehaviour
{
	#region Properties

	public static BannerManager Get { get; private set; } = null;

	#endregion


	#region MonoBehaviour

	private void Awake()
	{
		Get = this;
	}

	private void OnDestroy()
	{
		Get = null;
	}

	#endregion


	#region Methods

	public void StartBanner()
	{
		Novulo.Utility.CoroutineHelper.Start(this, UpdateBanner());
	}

	#endregion


	#region Coroutine

	private IEnumerator UpdateBanner()
	{
		Novulo.Adverts.AdsManager.Get.LoadBanner();
		//
		while (!PlayerProfile.Data.RemoveAds)
		{
			do
			{ yield return new WaitForSeconds(0.25f); }
			while (!Novulo.Adverts.AdsManager.Get.IsBannerReady());
			//
			Novulo.Adverts.AdsManager.Get.ShowBanner();
			//
			do
			{ yield return new WaitForSeconds(0.25f); }
			while (Novulo.Adverts.AdsManager.Get.IsBannerShowing() && !PlayerProfile.Data.RemoveAds);
		}
		//
		Novulo.Adverts.AdsManager.Get.HideBanner();
	}

	#endregion
}
