﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProfile : MonoBehaviour
{
	#region Types
	[System.Serializable]
	public class PlayerData
	{
		#region Members - DO NOT CHANGE THE ORDER OF MEMBERS

		public bool GDPRConsent = false;

		// Audio settings
		public bool SoundOn = true;

		//
		public bool RemoveAds = false;

		//
		public Novulo.Localisation.Translator.ELang GameLanguage = Novulo.Localisation.Translator.ELang.Noof;

		//
		public uint FreeModeBest = 0;
		public uint TimedAttackBest = 0;

		public double FreeBestTime = 0;

		#endregion
	}

	#endregion


	#region Members

	private const string SAVEFILE = "save.bin";
	private bool m_loaded = false;
	private bool m_dirty = false;

	#endregion


	#region Properties

	public static PlayerProfile Get { get; private set; } = null;

	public static PlayerData Data { get; private set; } = null;

	#endregion


	#region MonoBehaviour

	private void Awake()
	{
		Get = this;
	}

	private void OnDestroy()
	{
		Get = null;
	}

	private void Start()
	{
		StartCoroutine(UpdateCoroutine());
	}

	private void OnApplicationPause(bool pause)
	{
		if (!pause)
			return;

		if (!m_loaded)
			return;

		// Save on going to background
		Save();
	}

	private void OnApplicationQuit()
	{
		if (!m_loaded)
			return;

		// Save on going to background
		Save();
	}

	#endregion


	#region Load/Save

	public static void Load()
	{
		string fullPath = System.IO.Path.Combine(Application.persistentDataPath, SAVEFILE);
		PlayerData loadedData = Novulo.IO.DiskIO.ReadData<PlayerData>(fullPath);
		if (loadedData == null)
			loadedData = new PlayerData();

		//
		Data = loadedData;

		//
		Get.m_loaded = true;
	}

	private static void Save()
	{
		string fullPath = System.IO.Path.Combine(Application.persistentDataPath, SAVEFILE);
		Novulo.IO.DiskIO.WriteData<PlayerData>(fullPath, Data);
	}

	#endregion


	#region Methods

	public void SetDirty()
	{
		m_dirty = true;
	}

	#endregion


	#region Coroutine

	private IEnumerator UpdateCoroutine()
	{
		while (true)
		{
			if (m_dirty)
			{
				Save();
				m_dirty = false;
			}

			//
			yield return new WaitForSeconds(3f);
		}
	}

	#endregion
}
