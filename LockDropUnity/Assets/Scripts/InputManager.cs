﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
	#region Members

	private Vector2 m_startPos = Vector2.zero;
	private Vector2 m_endPos = Vector2.zero;

	#endregion


	#region MonoBehaviour

    void Update()
    {
		if (null == FloorController.SelectedFloor)
			return;

#if UNITY_EDITOR

		if (Input.GetMouseButtonDown(0))
		{
			m_startPos = Input.mousePosition;
		}
		else if (Input.GetMouseButton(0))
		{
			m_endPos = Input.mousePosition;

			// Find delta since last frame
			Vector2 delta = m_endPos - m_startPos;

			// Move floor
			FloorController.SelectedFloor.MoveFloor(delta);

			// Update last position
			m_startPos = m_endPos;
		}
		else if (Input.GetMouseButtonUp(0))
		{
			m_startPos = Vector2.zero;
			m_endPos = Vector2.zero;
		}

#else

		if (Input.touchCount > 0)
		{
			Touch t = Input.GetTouch(0);

			switch (t.phase)
			{
				case TouchPhase.Began:
					{
						m_startPos = Input.mousePosition;
					}
					break;

				case TouchPhase.Ended:
					{
						m_startPos = Vector2.zero;
						m_endPos = Vector2.zero;
					}
					break;

				case TouchPhase.Moved:
					{
						m_endPos = Input.mousePosition;

						// Find delta since last frame
						Vector2 delta = m_endPos - m_startPos;

						// Move floor
						FloorController.SelectedFloor.MoveFloor(delta);

						// Update last position
						m_startPos = m_endPos;
					}
					break;
			}
		}
#endif
	}

	#endregion
}
