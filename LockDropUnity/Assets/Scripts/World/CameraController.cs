﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	#region Members

	public float MinY;

	#endregion


	#region MonoBehaviour

	private void Awake()
	{
		LevelManager.OnLevelStart += LevelStart;
		LevelManager.OnLevelEnded += LevelEnded;
	}

	private void OnDestroy()
	{
		LevelManager.OnLevelStart -= LevelStart;
		LevelManager.OnLevelEnded -= LevelEnded;
	}


	#endregion


	#region Callback

	private void LevelStart()
	{
		Novulo.Utility.CoroutineHelper.Start(this, UpdateCoroutine());
	}

	private void LevelEnded()
	{
		StopAllCoroutines();
	}

	#endregion


	#region Coroutine

	private IEnumerator UpdateCoroutine()
	{
		while (true)
		{
			yield return new WaitForEndOfFrame();
			//
			float y = BallController.Get.transform.position.y;
			y = Mathf.Max(MinY, y);
			//
			Vector3 pos = transform.position;
			pos.y = y;
			transform.position = pos;
		}
	}

	#endregion
}
