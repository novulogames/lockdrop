﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorController : MonoBehaviour
{
	#region Members

	public float RotationSpeed;
	[Space]
	public GameObject[] Holes;
	public BoxCollider[] Colliders;
	[Space]
	public AudioSource Source;

	private bool m_selected = false;
	private bool m_enabled = true;

	#endregion


	#region Properties

	public static FloorController SelectedFloor { get; set; } = null;

	#endregion


	#region MonoBehaviour

	private void Awake()
	{
		LevelManager.OnLevelEnded += LevelEnded;
	}

	private void OnDestroy()
	{
		LevelManager.OnLevelEnded -= LevelEnded;
	}

	private void Start()
	{
		int rnd = Random.Range(0, 999) % Holes.Length;
		Holes[rnd].SetActive(false);

		//

		int angle = (2 * rnd + 1) * 45;
		transform.rotation = Quaternion.Euler(0f, angle, 0f);

		//

		Source.mute = !PlayerProfile.Data.SoundOn;
		Novulo.Utility.CoroutineHelper.Start(this, UpdateSound());
	}

	private void OnMouseDown()
	{
		if (false == m_enabled)
			return;
		if (null != SelectedFloor)
			return;

		SelectedFloor = this;
		m_selected = true;
	}

	private void OnMouseUp()
	{
		if (false == m_selected)
			return;

		m_selected = false;
		SelectedFloor = null;
	}

	#endregion


	#region Methods

	public void MoveFloor(Vector2 delta)
	{
		if (LevelManager.Get.GamePaused)
			return;
		if (false == m_enabled)
			return;

		transform.Rotate(new Vector3(0f, -delta.x * Time.deltaTime * RotationSpeed, 0f));
	}

	public void DisableFloor()
	{
		m_enabled = false;
		m_selected = false;
		SelectedFloor = null;
		//
		foreach (BoxCollider bc in Colliders)
		{ bc.enabled = false; }
		//
		transform.rotation = Quaternion.identity;
		for (int idx = 0; idx < Holes.Length; ++idx)
		{ Holes[idx].SetActive(idx != 0); }
		//
		StopAllCoroutines();
		Source.Pause();
	}

	#endregion


	#region Callback

	private void LevelEnded()
	{
		DisableFloor();
	}

	#endregion


	#region Coroutine

	private IEnumerator UpdateSound()
	{
		float oldY = transform.rotation.eulerAngles.y;
		float newY = transform.rotation.eulerAngles.y;

		Source.Play();
		Source.Pause();

		while (true)
		{
			oldY = transform.rotation.eulerAngles.y;
			yield return new WaitForEndOfFrame();
			newY = transform.rotation.eulerAngles.y;

			//

			if (oldY != newY)
				Source.UnPause();
			else
				Source.Pause();
		}
	}

	#endregion
}
