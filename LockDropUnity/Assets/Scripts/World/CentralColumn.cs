﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentralColumn : MonoBehaviour
{
	#region MonoBehaviour

	private void Awake()
	{
		LevelManager.OnLevelCreated += LevelCreate;
	}

	private void OnDestroy()
	{
		LevelManager.OnLevelCreated -= LevelCreate;
	}

	#endregion


	#region Callback

	private void LevelCreate()
	{
		float offset = LevelManager.Get.CurrentLevel * 5f;
		transform.localScale	= new Vector3(1f, offset, 1f);
		transform.position		= new Vector3(0f, offset, 0f); 
	}

	#endregion
}
