﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorHelper : MonoBehaviour
{
	#region Members

	public FloorController Parent;

	#endregion


	#region MonoBehaviour

	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("ball"))
		{
			AudioManager.Get.PlayDropSound();
			Parent.DisableFloor();
		}
	}

	#endregion
}
