﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
	#region Properties

	public static BallController Get { get; private set; } = null;

	#endregion


	#region MonoBehaviour

	private void Awake()
	{
		Get = this;
	}

	private void OnDestroy()
	{
		Get = null;
	}

	#endregion
}
