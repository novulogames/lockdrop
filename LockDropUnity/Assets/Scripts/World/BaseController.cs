﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseController : MonoBehaviour
{
	#region Events

	public delegate void LevelCompleteCallback();
	public static LevelCompleteCallback OnLevelComplete;

	#endregion


	#region MonoBehaviour

	private void OnCollisionEnter(Collision collision)
	{
		if (string.Equals(collision.gameObject.tag, "ball"))
		{
			AudioManager.Get.PlayLevelCompleteSound();
			OnLevelComplete?.Invoke();
		}
	}

	#endregion
}
