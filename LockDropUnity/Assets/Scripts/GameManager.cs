﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	#region Events

	public delegate void GameInitCallback();
	public static GameInitCallback OnGameInit;

	#endregion


	#region Properties

	public static GameManager Get { get; private set; } = null;

	#endregion


	#region MonoBehaviour

	private void Awake()
	{
		Novulo.Store.IAPManager.OnIAPPurchaseComplete += IAPComplete;
		//
		Get = this;
	}

	private void OnDestroy()
	{
		Get = null;
		//
		Novulo.Store.IAPManager.OnIAPPurchaseComplete -= IAPComplete;
	}

	private void Start()
	{
		Application.targetFrameRate = 60;
		StartCoroutine(DelayGameStart());
	}

	#endregion


	#region Methods

	public void ProcessIAP(string IAPName)
	{
		if (string.Equals(IAPName, "lockdrop.removeads"))
		{
			PlayerProfile.Data.RemoveAds = true;
			PlayerProfile.Get.SetDirty();
		}
	}

	public void LoadGame()
	{
		if (PlayerProfile.Data.GameLanguage == Novulo.Localisation.Translator.ELang.Noof)
		{
			SceneHandler.LoadScene(SceneHandler.EScene.Language);
		}
		else
		{
			Novulo.Localisation.Translator.Get.SetLanguage(PlayerProfile.Data.GameLanguage);
			SceneHandler.LoadScene(SceneHandler.EScene.Game);
		}
	}

	public string FormatToDisplay(double input)
	{
		System.TimeSpan time = System.TimeSpan.FromSeconds(input);
		System.Text.StringBuilder builder = new System.Text.StringBuilder();

		if (time.TotalMinutes < 1f)
		{
			builder.Insert(0, string.Format("<size=20>{0:00}.</size><size=10>{1:00}</size>", time.Seconds, time.Milliseconds / 10));
		}
		else
		{
			builder.Insert(0, string.Format("<size=20>{0:00}\t:\t{1:00}</size>", time.Minutes + (time.Hours * 60f), time.Seconds));
		}

		return builder.ToString();
	}

	public string FormatToUI(double input)
	{
		System.TimeSpan time = System.TimeSpan.FromSeconds(input);
		System.Text.StringBuilder builder = new System.Text.StringBuilder();

		if (time.TotalMinutes < 1f)
		{
			builder.Insert(0, string.Format("{0:00}.{1:00}", time.Seconds, time.Milliseconds / 10));
		}
		else
		{
			builder.Insert(0, string.Format("{0:00}:{1:00}", time.Minutes + (time.Hours * 60f), time.Seconds));
		}

		return builder.ToString();
	}

	#endregion


	#region Callback

	private void IAPComplete(string IAPName, bool success)
	{
		if (success)
		{
			ProcessIAP(IAPName);
		}
	}

	#endregion


	#region Coroutine

	private IEnumerator DelayGameStart()
	{
		yield return new WaitForEndOfFrame();

		PlayerProfile.Load();
		//
		Novulo.Store.IAPManager.Get.Init();
		//
		yield return new WaitForSeconds(2f);

		//

		OnGameInit?.Invoke();

		//

		if (!PlayerProfile.Data.GDPRConsent)
		{
			SceneHandler.LoadScene(SceneHandler.EScene.GDPR);
			yield break;
		}

		//
		Novulo.Adverts.AdsManager.Get.GiveGDPRConcent(PlayerProfile.Data.GDPRConsent);
		LoadGame();
	}

	#endregion
}
