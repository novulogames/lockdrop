﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
	#region Members

	public AudioSource UISource;
	[Space]
	public AudioClip PressSound;
	public AudioClip CancelSound;
	public AudioClip GoSound;
	public AudioClip TickSound;
	public AudioClip LevelCompleteSound;
	public AudioClip DropSound;

	#endregion


	#region Properties

	public static AudioManager Get { get; private set; } = null;

	#endregion


	#region MonoBehaviour

	private void Awake()
	{
		Get = this;
	}

	private void OnDestroy()
	{
		Get = null;
	}

	#endregion


	#region Methods

	public void Mute(bool mute)
	{
		UISource.mute = mute;
	}

	#endregion


	#region Callback

	public void PlayPressSound()
	{
		if (!PlayerProfile.Data.SoundOn)
			return;

		UISource.PlayOneShot(PressSound);
	}

	public void CancelPressSound()
	{
		if (!PlayerProfile.Data.SoundOn)
			return;

		UISource.PlayOneShot(CancelSound);
	}

	public void PlayGoSound()
	{
		if (!PlayerProfile.Data.SoundOn)
			return;

		UISource.PlayOneShot(GoSound);
	}

	public void PlayTickSound()
	{ 
		if (!PlayerProfile.Data.SoundOn)
			return;

		UISource.PlayOneShot(TickSound);
	}

	public void PlayLevelCompleteSound()
	{
		if (!PlayerProfile.Data.SoundOn)
			return;

		UISource.PlayOneShot(LevelCompleteSound, 0.15f);
	}

	public void PlayDropSound()
	{
		if (!PlayerProfile.Data.SoundOn)
			return;

		UISource.PlayOneShot(DropSound, 0.7f);
	}

	#endregion
}
