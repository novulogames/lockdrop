﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameMenu : MonoBehaviour
{
	#region Types

	public delegate void LeaveLevelCallback();
	public static LeaveLevelCallback OnLeaveLevel;

	#endregion


	#region Members

	public GameObject Group;
	[Space]
	public Text LevelLabel;
	public Text TimeLabel;
	public GameObject InfoPanel;
	[Space]
	public GameObject LeaveButton;

	#endregion


	#region MonoBehaviour

	private void Awake()
	{
		MainMenu.OnGameSelected += GameSelected;
	}

	private void OnDestroy()
	{
		MainMenu.OnGameSelected -= GameSelected;
	}

	private void Start()
	{
		Group.SetActive(false);
	}

	#endregion


	#region Internal

	private void UpdateDisplay()
	{
		TimeLabel.text = GameManager.Get.FormatToDisplay(LevelManager.Get.CurrentTime);
	}

	private void EnableElements(bool enable)
	{
		InfoPanel.SetActive(enable);
		LeaveButton.SetActive(enable);
	}

	#endregion


	#region Callback

	public void LeaveButtonPressed()
	{
		LevelManager.Get.GamePaused = true;
		//
		CountdownController.OnCountdownOver -= CountdownOver;
		BaseController.OnLevelComplete -= LevelComplete;
		//
		Group.SetActive(false);
		Novulo.Utility.CoroutineHelper.Start(this, DelayLeave());
	}

	private void GameSelected(Types.EGameMode mode)
	{
		CountdownController.OnCountdownOver += CountdownOver;
		BaseController.OnLevelComplete += LevelComplete;
		//
		Group.SetActive(true);
		EnableElements(false);
	}

	private void CountdownOver()
	{
		EnableElements(true);
		//
		Novulo.Utility.CoroutineHelper.Start(this, UpdateCoroutine());
	}

	private void LevelComplete()
	{
		EnableElements(false);
	}

	#endregion


	#region Coroutine

	private IEnumerator UpdateCoroutine()
	{
		while (true)
		{
			LevelLabel.text = LevelManager.Get.CurrentLevel.ToString();
			UpdateDisplay();
			//
			yield return new WaitForEndOfFrame();
		}
	}

	private IEnumerator DelayLeave()
	{
		bool showAds = true;
		showAds &= (PlayerProfile.Data.RemoveAds == false);
		showAds &= Novulo.Adverts.AdsManager.Get.IsAdReady(Novulo.Adverts.AdsManager.AdType.Video);
		if (showAds)
		{
			Novulo.Adverts.AdsManager.Get.ShowAd(Novulo.Adverts.AdsManager.AdType.Video, null, null);
			yield return new WaitForSeconds(1f);
		}

		//
		OnLeaveLevel?.Invoke();
	}

	#endregion
}
