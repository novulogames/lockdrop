﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GDPRMenu : MonoBehaviour
{
	#region Callbacks

	public void ReadPressed()
	{
		Application.OpenURL("http://novulo.games/privacy-policy/");
	}

	public void AcceptPressed()
	{
		PlayerProfile.Data.GDPRConsent = true;
		PlayerProfile.Get.SetDirty();
		Novulo.Adverts.AdsManager.Get.GiveGDPRConcent(PlayerProfile.Data.GDPRConsent);
		//
		GameManager.Get.LoadGame();
	}

	#endregion
}
