﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler
{
	#region Types

	public enum EScene
	{
		Splash,
		GDPR,
		Language,
		Game,

		Noof
	}

	#endregion


	#region Members

	private static Dictionary<EScene, int> SceneMap = new Dictionary<EScene, int>()
	{
		{ EScene.Splash		, 0 },
		{ EScene.GDPR		, 1 },
		{ EScene.Language	, 2 },
		{ EScene.Game		, 3 },
	};

	#endregion


	#region Methods

	public static void LoadScene(EScene scene)
	{
		SceneManager.LoadScene(SceneMap[scene]);
	}

	#endregion
}
