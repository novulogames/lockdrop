﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

#if GPGS
using GooglePlayGames;
using GooglePlayGames.BasicApi;
#endif

#if UNITY_IOS
using UnityEngine.SocialPlatforms.GameCenter;
#endif

namespace Novulo.Social
{
	public class SocialManager : MonoBehaviour
	{
		#region Evetns

		public delegate void LoginCallback(bool success, string error);
		public static LoginCallback OnLogin;

		public delegate void LeaderboardPostCallback(bool success);
		public static LeaderboardPostCallback OnLeaderboardPost;

		public delegate void AchievementPostCallback(bool success);
		public static AchievementPostCallback OnAchievementPost;

		#endregion


		#region Properties

		public static SocialManager Get { get; private set; } = null;

		public bool IsLoggedIn
		{
			get { return UnityEngine.Social.localUser.authenticated; }
		}

		#endregion


		#region MonoBehaviour

		private void Awake()
		{
			Get = this;
		}

		private void OnDestroy()
		{
			Get = null;
		}

		private void Start()
		{
			Initialize();
		}

		#endregion


		#region Methods

		public void LogIn()
		{
#if UNITY_AMAZON || UNITY_SOS
            return;
#endif

			UnityEngine.Social.localUser.Authenticate(AuthenticateCallback);
		}

		public void ShowLeaderboardsUI(string boardID = "")
		{
#if UNITY_AMAZON || UNITY_SOS
            return;
#endif
#if UNITY_ANDROID && GPGS
			if (string.IsNullOrEmpty(boardID))
				UnityEngine.Social.ShowLeaderboardUI();
			else
				PlayGamesPlatform.Instance.ShowLeaderboardUI(boardID);
#elif UNITY_IOS
            GameCenterPlatform.ShowLeaderboardUI(boardID, UnityEngine.SocialPlatforms.TimeScope.AllTime);
#endif
		}

		public void ShowAchievementsUI()
		{
#if UNITY_AMAZON || UNITY_SOS
            return;
#endif
			UnityEngine.Social.ShowAchievementsUI();
		}

		public void PublishLeaderboardEntry(string boardID, int points)
		{
#if UNITY_AMAZON || UNITY_SOS
            return;
#endif

			if (!IsLoggedIn)
				return;

			if (points <= 0)
			{
				Debug.Assert(false, "Leaderboard points cannot be negative");
				return;
			}

			//
			UnityEngine.Social.ReportScore(points, boardID, LeaderboardEntryPosted);
		}

		public void UnlockAchievement(string achievementID)
		{
#if UNITY_AMAZON || UNITY_SOS
            return;
#endif

			if (!IsLoggedIn)
				return;

			UnityEngine.Social.ReportProgress(achievementID, 100.0f, AchievementReported);
		}

		public void UpdateAchievement(string achievementID, double progress)
		{
#if UNITY_AMAZON || UNITY_SOS
            return;
#endif

			if (!IsLoggedIn)
				return;

			if (progress < 0d)
			{
				Debug.Assert(false, "Achievment progress cannot be negative");
				return;
			}

			UnityEngine.Social.ReportProgress(achievementID, progress, AchievementReported);
		}

		#endregion


		#region Internal

		private void Initialize()
		{
#if UNITY_AMAZON || UNITY_SOS
            return;
#endif

#if UNITY_ANDROID && GPGS
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
                // enables saving game progress.
                //.EnableSavedGames()
                //
                .Build();

            PlayGamesPlatform.InitializeInstance(config);
            // recommended for debugging:
#if DEBUG
            PlayGamesPlatform.DebugLogEnabled = true;
#endif
            // Activate the Google Play Games platform
            PlayGamesPlatform.Activate();

#elif UNITY_IOS
            GameCenterPlatform.ShowDefaultAchievementCompletionBanner(true);
#endif
		}

		#endregion


		#region Callbacks

		private void AuthenticateCallback(bool success, string message)
		{
			Debug.Log("Authentication " + (success ? "successful" : "failed") + " - " + (message ?? "no error"));

			if (OnLogin != null)
				OnLogin(success, message);
		}

		private void LeaderboardEntryPosted(bool success)
		{
			Debug.Log("Leaderboard entry " + (success ? "successful" : "failed"));
			if (OnLeaderboardPost != null)
				OnLeaderboardPost(success);
		}

		private void AchievementReported(bool success)
		{
			Debug.Log("Achievement unlock " + (success ? "successful" : "failed"));
			if (OnAchievementPost != null)
				OnAchievementPost(success);
		}

		#endregion
	}
}