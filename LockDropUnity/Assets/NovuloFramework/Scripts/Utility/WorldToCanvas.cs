﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Novulo.Utility
{
    public class WorldToCanvas : MonoBehaviour
    {
        #region Members

        public Canvas TargetCanvas;
        public RectTransform CanvasRect;
        public Camera TargetCamera;

		#endregion


		#region Properties

		public static WorldToCanvas Get { get; private set; } = null;

        #endregion


        #region MonoBehaviour

        private void Awake()
        {
            Get = this;
        }

        private void OnDestroy()
        {
            Get = null;
        }

        #endregion


        #region Methods

        public Vector2 WorldToCanvasPosition(Vector3 position)
        {
            Vector2 screenPoint = RectTransformUtility.WorldToScreenPoint(TargetCamera, position);
            Vector2 result;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(CanvasRect, screenPoint, TargetCanvas.renderMode == RenderMode.ScreenSpaceOverlay ? null : TargetCamera, out result);
            return TargetCanvas.transform.TransformPoint(result);
        }

        #endregion
    }
}