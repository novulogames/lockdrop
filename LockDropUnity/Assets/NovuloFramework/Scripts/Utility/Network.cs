﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Novulo.Utility
{
    public class Network : MonoBehaviour
    {
        #region Members

        public bool ForceOffline;

        #endregion


        #region Properties

        public static NetworkReachability Reachability { get; private set; } = NetworkReachability.NotReachable;

        #endregion


        #region MonoBehaviour

        private void Awake()
        {
            UpdateConnectivity();
        }

        private void Start()
        {
            StartCoroutine(UpdateNetworkReachabilityCoroutine());
        }

        #endregion


        #region Internal

        private void UpdateConnectivity()
        {
            Reachability = Application.internetReachability;
#if UNITY_EDITOR
            if (ForceOffline)
                Reachability = NetworkReachability.NotReachable;
#endif
        }

        #endregion


        #region Coroutine

        private IEnumerator UpdateNetworkReachabilityCoroutine()
        {
            while (true)
            {
                UpdateConnectivity();
                yield return new WaitForSeconds(3f);
            }
        }

        #endregion
    }
}
