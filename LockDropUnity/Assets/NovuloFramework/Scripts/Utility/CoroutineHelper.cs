﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Novulo.Utility
{
    public class CoroutineHelper
    {
        public static void Start(MonoBehaviour owner, IEnumerator coroutine)
        {
            if (owner == null)
            {
                Debug.LogError("Starting coroutine on nil object");
                return;
            }

            if (!owner.gameObject.activeSelf ||
                !owner.gameObject.activeInHierarchy)
            {
                string sceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
                string objectName = owner.gameObject.name;
                string scriptName = owner.GetType().ToString();
                string description = (!owner.gameObject.activeInHierarchy) ? "object inactive in hierarchy" : "disabled object";

                string warning = string.Format("Scene: {0} - GameObject: {1} - Script: {2} - Could not start coroutine on {3}.", sceneName, objectName, scriptName, description);
                Debug.LogWarning(warning);

                return;
            }

            // OK to start coroutine
            owner.StartCoroutine(coroutine);
        }
    }
}