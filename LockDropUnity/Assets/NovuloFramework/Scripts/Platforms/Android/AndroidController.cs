﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Novulo.Platform.Android
{
    public class AndroidController : MonoBehaviour
    {
        #region MonoBehaviour

#if !UNITY_ANDROID
        private void Awake()
        {
            Destroy(gameObject);
        }
#endif

        #endregion
    }
}