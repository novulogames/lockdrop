﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Novulo.Platform.Sailfish
{
    public class SailfishController : MonoBehaviour
    {
        #region MonoBehaviour

#if UNITY_SOS
        private void Awake()
        {
            Destroy(gameObject);
        }
#endif

        #endregion
    }
}