﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Novulo.Localisation
{
	[AddComponentMenu("UI/Translated Text")]
    public class TranslatedText : MonoBehaviour
    {
        #region Members

        public string StringID;

		private Text m_text;

		#endregion


		#region MonoBehaviour

		private void Awake()
		{
			m_text = GetComponent<Text>();
		}

		private void OnEnable()
        {
			UpdateState();
        }

		private void OnValidate()
		{
			if (string.IsNullOrEmpty(GetComponent<Text>().text))
				GetComponent<Text>().text = StringID;
		}

		#endregion


		#region Internal

		private void UpdateState()
		{
			// Translate text
			m_text.text = Translator.Get?.GetTranslation(StringID);
		}

		#endregion
	}
}