﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Novulo.Utility;

namespace Novulo.UI
{
    public class FadePanel : MonoBehaviour
    {
        #region Members

        public Color FadeColour;

        private Image m_panel;

        #endregion


        #region Properties

        public static FadePanel Get { get; private set; } = null;

        #endregion


        #region MonoBehaviour

        private void Awake()
        {
            m_panel = GetComponent<Image>();
            m_panel.enabled = true;
            //
            transform.SetAsLastSibling();
            //
            Get = this;
        }

        private void OnDestroy()
        {
            Get = null;
        }

        private void OnValidate()
        {
            GetComponent<Image>().enabled = false;
        }

        #endregion


        #region Methods

        public void FadeIn()
        {
            CoroutineHelper.Start(this, FadeInCoroutine(FadeColour));
        }

        public void FadeIn(Color c)
        {
            CoroutineHelper.Start(this, FadeInCoroutine(c));
        }

        public void FadeOut()
        {
            CoroutineHelper.Start(this, FadeOutCoroutine(FadeColour));
        }

        public void FadeOut(Color c)
        {
            CoroutineHelper.Start(this, FadeOutCoroutine(c));
        }

        #endregion


        #region Coroutine

        private IEnumerator FadeInCoroutine(Color fadeColor)
        {
            fadeColor.a = 1f;
            m_panel.color = fadeColor;
            m_panel.enabled = true;

            Color c = m_panel.color;
            float timer = 1f;
            while (timer >= 0f)
            {
                timer -= (Time.deltaTime * 2f);
                c.a = timer;
                m_panel.color = c;

                yield return new WaitForEndOfFrame();
            }

            m_panel.enabled = false;
        }

        private IEnumerator FadeOutCoroutine(Color fadeColor)
        {
            fadeColor.a = 0f;
            m_panel.color = fadeColor;
            m_panel.enabled = true;

            Color c = m_panel.color;
            float timer = 0f;
            while (timer <= 1f)
            {
                timer += (Time.deltaTime * 2f);
                c.a = timer;
                m_panel.color = c;

                yield return new WaitForEndOfFrame();
            }
        }

        #endregion
    }
}