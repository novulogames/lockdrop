﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Novulo.UI
{
	public class FPSCounter : MonoBehaviour
	{
        #region Members

        public Text FPSText;

        private int frames = 0; // Frames drawn over the interval

        #endregion


        #region MonoBehaviour

        void Start()
		{
			if (!FPSText)
			{
				Debug.Log("UtilityFramesPerSecond needs a GUIText component!");
				enabled = false;
				return;
			}

            Novulo.Utility.CoroutineHelper.Start(this, UpdateCounter());
		}

        private void OnRenderObject()
        {
            ++frames;
        }

        #endregion


        #region Coroutine

        private IEnumerator UpdateCounter()
		{
            while (true)
            {
                yield return new WaitForSecondsRealtime(1f);
                {
                    FPSText.text = frames.ToString();

                    if (frames < 30)
                        FPSText.color = Color.yellow;
                    else
                        if (frames < 10)
                        FPSText.color = Color.red;
                    else
                        FPSText.color = Color.green;

                    frames = 0;
                }
            }
		}

        #endregion
    }
}